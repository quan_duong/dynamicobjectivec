//
//  ViewController.m
//  DynamicObjectiveC
//
//  Created by Duong Tien Quan on 11/18/15.
//  Copyright © 2015 vtvcab. All rights reserved.
//

#import "ViewController.h"
#pragma mark - Animals
@interface Cat : NSObject
-(NSString *) className;
@end

@implementation Cat
-(NSString *) className
{
    return NSStringFromClass([self class]);
}
@end

@interface Dog : NSObject
-(NSString *) className;
@end

@implementation Dog
-(NSString *) className
{
    return NSStringFromClass([self class]);
}
@end

@interface Tiger : NSObject
-(NSString *) className;

@end

@implementation Tiger
-(NSString *) className
{
    return NSStringFromClass([self class]);
}
@end
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtAnimal;
@property (weak, nonatomic) IBOutlet UIImageView *animal;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)displayImage:(id)sender {
    NSLog(@"%@",_txtAnimal.text);
    Class class = NSClassFromString(_txtAnimal.text);
    if (class == nil) {
        NSLog(@"objext is not exit");
        //NSString* noexit = @"noexist";
        self.animal.image = [UIImage imageNamed:@"noexist"];
        return;
    }
    id object = [class new];
    NSLog(@"object name %@",[object className]);
    _animal.image = [UIImage imageNamed:[object className]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
